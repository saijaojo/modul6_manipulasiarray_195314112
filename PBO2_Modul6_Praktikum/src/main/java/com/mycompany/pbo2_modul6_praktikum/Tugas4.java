/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul6_praktikum;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author asus
 */

public class Tugas4 {

    public static void main(String[] args) {
        Scanner The = new Scanner(System.in);

        System.out.println("Masukan Kalimat : ");
        String input = The.nextLine();

        StringTokenizer tokens = new StringTokenizer(input);

        System.out.println("Jumlah kata : ");
        System.out.println(tokens.countTokens() + " kata");

    }

}

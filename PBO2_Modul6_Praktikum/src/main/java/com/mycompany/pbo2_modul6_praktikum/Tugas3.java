/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul6_praktikum;

import java.util.Scanner;


/**
 *
 * @author asus
 */

public class Tugas3 {
    public static void main(String[] args) {
        Scanner The = new Scanner(System.in);
        
        String input, output;
         System.out.println("Input kata : ");
         input = The.next();
         
         
         StringBuffer inputkata = new StringBuffer(input);
         
         StringBuffer reverse = new StringBuffer(inputkata.reverse());
         
         System.out.println("Output : "+reverse);
         output = reverse.toString();
         
         String status;
         
         if (input.equals(output)){
              status = "Palindrome";
         }else {
             status = "Bukan palindrome";
         }
         System.out.println("Status : "+ status);
    }
    
}

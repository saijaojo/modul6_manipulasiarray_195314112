/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

/**
 *
 * @author asus
 */
public class UKM {
    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekertaris;
    private Penduduk [] anggota =  new Penduduk [1];

    public UKM() {
    }

    public UKM(String namaUnit, Mahasiswa ketua, Mahasiswa sekertaris) {
        this.namaUnit = namaUnit;
        this.ketua = ketua;
        this.sekertaris = sekertaris;
    }
    
    String getNama_unit (){
        return namaUnit;
    }
    void setNama_unit (String dataNama_unit){
        this.namaUnit =dataNama_unit;
    }
    Mahasiswa getKetua (){
        return ketua;
    }
    void setKetua (Mahasiswa dataKetua){
        this.ketua = dataKetua;
    }
    Mahasiswa getSekertaris (){
        return sekertaris;
    }
    void setSekertaris (Mahasiswa dataSekertaris){
        this.sekertaris =dataSekertaris;
    }
    Penduduk [] getAnggota (){
        return anggota;
    }
    void setAnggota (Penduduk [] dataPenduduk){
        this.anggota =dataPenduduk;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iiic;


import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JDialog;

/**
 *
 * @author asus
 */

public class Nomor2 extends JDialog{
    public static final int DIALOG_WIDTH = 500;
    public static final int DIAOLOG_HEIGHT = 250;
    public static final int BUTTON_WIDTH = 120;
    public static final int BUTTON_HEIGHT = 30;
    private JButton YELLOW;
    private JButton BLUE;
    private JButton RED;
    
    public static void main(String[] args) {
        Nomor2 dialog = new Nomor2();
        dialog.setVisible(true);
    }

    public Nomor2() {
        Container contentPane  =  getContentPane();
        setSize(DIALOG_WIDTH, DIAOLOG_HEIGHT);
        setLocation(100, 100);
        setResizable(true);
        setTitle("ButtonTest");
        contentPane.setLayout(null);
        //contentPane.setBackground(Color.white);
        
        YELLOW = new JButton("YELLOW");
        YELLOW.setBounds(50, 5, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(YELLOW);
        
        BLUE = new JButton("BLUE");
        BLUE.setBounds(190, 5, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(BLUE);
        
        RED = new JButton("RED");
        RED.setBounds(330, 5, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(RED);
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
    }
}

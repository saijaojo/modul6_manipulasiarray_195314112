/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iiic;

import java.awt.Container;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Nomor3 extends JDialog {

    private static final int DIALOG_WIDTH = 400;
    private static final int DIALOG_HEIGHT = 200;
    private JLabel gmbr;
    private JLabel text;
    

    public static void main(String[] args) {
        Nomor3 dialog = new Nomor3();
        dialog.setVisible(true);
    }

    public Nomor3() {
        Container contentPane = getContentPane();
        setSize(DIALOG_WIDTH, DIALOG_HEIGHT);
        setResizable(true);
        setLocation(100, 100);
        setTitle("Text adn Icon Label");
        contentPane.setLayout(null);
        //contentPane.setBackground(Color.white);

        ImageIcon image = new ImageIcon("nmr3.jpg");

        
        gmbr = new JLabel();
        gmbr.setIcon(image);
        gmbr.setBounds(150, 20, 100, 100);
        contentPane.add(gmbr);

        text = new JLabel("Grape");
        text.setBounds(175, 110, 100, 50);
        contentPane.add(text);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }

}

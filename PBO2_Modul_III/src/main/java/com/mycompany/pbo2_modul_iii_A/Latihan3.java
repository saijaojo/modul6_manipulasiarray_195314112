/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iii_A;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class Latihan3 extends JFrame{
    public Latihan3 (){
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini class turunan dari class JFrame");
        this.setVisible(true);
        
        JPanel panel =new JPanel(null);
        JButton tombol = new JButton();
        JLabel label = new JLabel();
        JTextField text = new JTextField();
        JCheckBox box = new  JCheckBox();
        JRadioButton radio = new JRadioButton();
        
        
        tombol.setText("Ini tombol");
        tombol.setBounds(150, 20, 110, 20);
        panel.add(tombol);
        this.add(panel);
        
        label.setText("Ini label");
        label.setBounds(150, 50, 150, 20);
        panel.add(label);
        this.add(panel);
        
        text.setText("ini text");
        text.setBounds(150, 80, 190, 20);
        panel.add(text);
        this.add(panel);
        
        box.setText("ini Box");
        box.setBounds(150, 110, 230, 20);
        panel.add(box);
        this.add(panel);
        
        radio.setText("ini radio");
        radio.setBounds(150, 140, 260, 20);
        panel.add(radio);
        this.add(panel);
        
    }
    public static void main(String[] args) {
        new Latihan3();
    }
}
